# Copyright (c) 2022 Proton Technologies AG
#
# This file is part of ProtonMail Bridge.
#
# ProtonMail Bridge is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ProtonMail Bridge is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ProtonMail Bridge.  If not, see <https://www.gnu.org/licenses/>.

---
include:
  - project: accessable-net/gitlab-ci-templates
    file: scheduled_rebase.yml
  - project: accessable-net/gitlab-ci-templates
    file: ci-image.yml

# Enable pipelines on merge requests, tags, and branches.
# Avoid duplicate pipelines when pushing to a branch associated with an open MR.
# https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
    - if: $CI_COMMIT_TAG

image: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

variables:
  GOPRIVATE: gitlab.protontech.ch
  GOMAXPROCS: $(( ${CI_TAG_CPU} / 2 ))

before_script:
  - apt update && apt-get -y install libsecret-1-dev
  - git config --global url.https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}.insteadOf https://${CI_SERVER_HOST}

stages:
  - environment
  - cache
  - test
  - build
  - deploy


.rules-branch-and-MR-always:
  rules:
    - if: $CI_COMMIT_BRANCH ||  $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
      allow_failure: false
    - when: never

.rules-branch-and-MR-manual:
  rules:
    - if: $CI_COMMIT_BRANCH ||  $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
      allow_failure: true
    - when: never

.rules-branch-manual-MR-always:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: always
      allow_failure: false
    - if: $CI_COMMIT_BRANCH
      when: manual
      allow_failure: true
    - when: never

.rules-never:
  rules:
    - when: never

# Stage: ENVIRONMENT
.updateContainerJob:
  before_script: []
  variables:
    DOCKER_FILE: ci/Dockerfile

# Stage: CACHE

# This will ensure latest dependency versions and updates the cache for
# all other following jobs which only pull the cache.
cache-push:
  stage: cache
  extends:
    - .rules-branch-and-MR-always
  script:
    - echo ""
  cache:
    key: go18-mod
    paths:
      - .cache

# Stage: TEST

lint:
  stage: test
  extends:
    - .rules-never
  before_script:
    - mkdir -p .cache/bin
    - export PATH=$(pwd)/.cache/bin:$PATH
    - export GOPATH="$CI_PROJECT_DIR/.cache"
  script:
    - make lint

test-linux:
  stage: test
  extends:
    - .rules-never
  script:
    - make test
  tags:
    - saas-linux-medium-amd64

test-linux-race:
  stage: test
  extends:
    - .rules-never
  script:
    - make test-race

test-integration:
  stage: test
  extends:
    - .rules-never
  script:
    - make test-integration

test-integration-race:
  stage: test
  extends:
    - .rules-branch-and-MR-manual
  script:
    - make test-integration-race

dependency-updates:
  stage: test
  script:
    - make updates

# Stage: BUILD

.build-base:
  stage: build
  before_script:
    - mkdir -p .cache/bin
    - export PATH=$(pwd)/.cache/bin:$PATH
    - export GOPATH="$CI_PROJECT_DIR/.cache"
  script:
    - make build
    - git diff && git diff-index --quiet HEAD
    - make vault-editor
  artifacts:
    # Note: The latest artifacts for refs are locked against deletion, and kept
    # regardless of the expiry time. Introduced in GitLab 13.0 behind a
    # disabled feature flag, and made the default behavior in GitLab 13.4.
    expire_in: 1 day
    when: always
    paths:
      - bridge_*.tgz
      - vault-editor

build-linux:
  extends: .build-base
  variables:
    VCPKG_DEFAULT_BINARY_CACHE: ${CI_PROJECT_DIR}/.cache
  cache:
    key: linux-vcpkg
    paths:
      - .cache
    when: 'always'
  artifacts:
    name: "bridge-linux-$CI_COMMIT_SHORT_SHA"
  # Use large SaaS machine for performance
  tags: [ saas-linux-large-amd64 ]

build-linux-qa:
  extends: 
    - build-linux
    - .rules-never
  variables:
    BUILD_TAGS: "build_qa"
  artifacts:
    name: "bridge-linux-qa-$CI_COMMIT_SHORT_SHA"


.build-windows-base:
  extends:
    - .build-base
    - .rules-never
  before_script:
    - export GOROOT=/c/Go1.18/
    - export PATH=$GOROOT/bin:$PATH
    - export GOARCH=amd64
    - export GOPATH=~/go18
    - export GO111MODULE=on
    - export PATH="${GOPATH}/bin:${PATH}"
    - export MSYSTEM=
    - export QT6DIR=/c/grrrQt/6.3.1/msvc2019_64
    - export PATH=$PATH:${QT6DIR}/bin
    - export PATH="/c/Program Files/Microsoft Visual Studio/2022/Community/Common7/IDE/CommonExtensions/Microsoft/CMake/CMake/bin:$PATH"
    - $(git config --global -l | grep -o 'url.*gitlab.protontech.ch.*insteadof' | xargs -L 1 git config --global --unset &> /dev/null) || echo "nothing to remove"
    - git config --global url.https://gitlab-ci-token:${CI_JOB_TOKEN}@${CI_SERVER_HOST}.insteadOf https://${CI_SERVER_HOST}
  script:
    - make build-nogui
    - git diff && git diff-index --quiet HEAD
    - make vault-editor
  tags:
    - windows-bridge

build-windows:
  extends: .build-windows-base
  artifacts:
    name: "bridge-windows-$CI_COMMIT_SHORT_SHA"

build-windows-qa:
  extends: .build-windows-base
  variables:
    BUILD_TAGS: "build_qa"
  artifacts:
    name: "bridge-windows-qa-$CI_COMMIT_SHORT_SHA"

# TODO: PUT BACK ALL THE JOBS! JUST DID THIS FOR NOW TO GET CI WORKING AGAIN...

# https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html?tab=Multi-project+pipeline
deploy-flatpak:
  stage: deploy
  variables:
    UPSTREAM_REF: $CI_MERGE_REQUEST_REF_PATH || $CI_COMMIT_BRANCH
  trigger:
    project: secure-system/proton/proton-bridge-flatpak
